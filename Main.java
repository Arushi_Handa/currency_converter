import java.util.Scanner;
public class Main
 {
public static void main(String[] args)
 {
		Scanner sc = new Scanner(System.in);
		String destCurrency;
        System.out.println("Enter the destination currency: ");
	    destCurrency=sc.nextLine();
		double usdQty;
        System.out.println("Enter the quantity to be converted: ");
        usdQty= sc.nextDouble();
		boolean d = validate(usdQty,destCurrency);
        if (d == true) {
        ConverterClass convertCurrency = new ConverterClass(usdQty,destCurrency);
		convertCurrency.display();
      	} else 
        {
		System.out.println("Unable to convert the given input.");
		}
 }
static boolean validate(double usdQty, String destCurrency)
 {
if ((usdQty > 0)||((destCurrency.equalsIgnoreCase("EUR")) || (destCurrency.equalsIgnoreCase("INR")) || (destCurrency.equalsIgnoreCase("MYR"))|| (destCurrency.equalsIgnoreCase("SGD")) || (destCurrency.equalsIgnoreCase("GBP")) || (destCurrency.equalsIgnoreCase("CAD"))))
return true;
else
return false;
}
}
class USDClass {
double usdQty;
	USDClass(double usdQty) {
    this.usdQty = usdQty;
	}
}
class ConverterClass extends USDClass {
	double f;
    String destCurrency;
	ConverterClass(double usdQty, String destCurrency) {
	super(usdQty);
	this.destCurrency = destCurrency;
}
	double convertCurrency() {
	if (destCurrency.equalsIgnoreCase("EUR")) {
        	return (double)(usdQty * 0.81);
		} else if (destCurrency.equalsIgnoreCase("INR")) {
			return (double)(usdQty * 64.31);
    	} else if (destCurrency.equalsIgnoreCase("MYR")) {
		return (double)(usdQty * 3.95);
		} else if (destCurrency.equalsIgnoreCase("SGD")) {
			return (double)(usdQty * 1.32);
		} else if (destCurrency.equalsIgnoreCase("GBP")) {
			return (double)(usdQty * 0.72);
		} else if (destCurrency.equalsIgnoreCase("CAD")) {
			return (double)(usdQty * 1.26);
		} else
			return (double)usdQty;
	}
	void display()
    {
	System.out.println("The " + destCurrency + " amount equivalent to " + usdQty + " is: " + convertCurrency());
	}
}
